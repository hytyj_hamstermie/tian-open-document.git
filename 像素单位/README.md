# 像素单位vp

屏幕密度相关像素，根据屏幕像素密度转换为屏幕物理像素。

屏幕密度DPI范围值`120-640`。

> - 分辨率：是指宽度上和高度上最多能显示的物理像素点个数。
> - DPI(屏幕像素密度)：即每英寸(1英寸=2.54厘米)聚集的像素点个数，这里的一英寸还是对角线长度。
> - DPR(设备像素比)：设备像素比 = 设备像素 / 设备独立像素。
> - 框架采用vp为基准数据单位（不指定单位时，默认单位vp）

## 单位转换

| 接口                           | 描述                                   |
| ------------------------------ | -------------------------------------- |
| vp2px(value : number) : number | 将vp单位的数值转换为以px为单位的数值。 |
| px2vp(value : number) : number | 将px单位的数值转换为以vp为单位的数值。 |

## 获取当前设备DPR

通过display接口，我们可以知道当前设备宽度的物理像素值`width_px`。

通过单位转换方法，获取当前设备宽度的密度相关像素值`width_vp`。

```typescript
width_vp = px2vp(width_px)
```

当前dpr计算

```typescript
dpr = width_px / px2vp(width_px)
```

在Previewer上进行尝试，我们可得以下对应关系。

> 在开发板上运行时，发现单位转换方法存在问题，已提交issue。
>
> https://gitee.com/openharmony/ace_ace_engine/issues/I4TFIF#note_8764185

## DPI 对应 DPR

|  | DPI  | 对应DPR |
| ---- | ---- | ------- |
| 表示屏幕密度小。 | 120  |   0.75      |
| 表示中等屏幕密度。 | 160  | 1 |
| 表示大屏幕密度。 | 240  | 1.5 |
| 表示超大屏幕密度。 | 320  | 2 |
| 表示超超大屏幕密度。 | 480  | 3 |
| 表示超超超大屏幕密度。 | 640  | 4 |

我们写两个块，一个使用px做单位，一个使用vp做单位。

```typescript
// DPI = 120

// vp
Column()
    .backgroundColor(Color.Blue)
    .width(100)
    .height(100)

// px
Column()
    .backgroundColor(Color.Red)
    .width(`${100 * 0.75}px`)
    .height(100)
```

两个块的宽度相等，以此类推。

![](./figures/1.png)

# 像素单位lpx

视窗逻辑像素单位，lpx单位为实际屏幕宽度与逻辑宽度(designWidth)的比值。如配置designWidth为720时，在实际宽度为1440物理像素的屏幕上，1lpx为2px大小。

屏幕自适应解决方案，这是由于开发板/手机宽度不尽相同，为了让一套代码适应任何尺寸的设备。

做过前端的小伙伴一定不陌生淘宝的flexible.js移动端自适应方案。

> flexible：仅对宽度做自适应计算。高度不自适应，一般适用于有滚动条的页面。这也是移动态最常见的通用做法。

lpx单位只需要专注设计稿尺寸，不需要考虑其他设备信息，即可以百分百实现界面与设计同步。

如果没有采用自适应是什么样呢？

```typescript
Column()
        .backgroundColor(Color.Blue)
        .width('360px')
        .height('100px')
```

- 720x1280
![](./figures/2.png)


- 960 x 1920
![](./figures/3.png)

发现蓝色块的宽高都不一样了。

那么我们使用自适应方案。

```typescript
Column()
        .backgroundColor(Color.Blue)
        .width('360lpx')
        .height('100lpx')
```
- 720x1280
![](./figures/4.png)


- 960 x 1920
![](./figures/5.png)

显示一样了！

## 单位转换

| 接口                            | 描述                                    |
| ------------------------------- | --------------------------------------- |
| lpx2px(value : number) : number | 将lpx单位的数值转换为以px为单位的数值。 |
| px2lpx(value : number) : number | 将px单位的数值转换为以lpx为单位的数值。 |

通过换算我们也可以获得一个`屏幕物理像素宽度width_px/设计宽度designWidth`的比值

```typescript
ratio = width_px / designWidth
```

> 发现单位转换方法存在问题，已提交issue。
>
> https://gitee.com/openharmony/ace_ace_engine/issues/I4UI87?from=project-issue

# 设备信息

## 3516信息
| Profile |  |
| ---- | ---- |
| Device type | phone |
| Resolution | 480 x 960 |
| DPI | 160 |

## RK3568信息
| Profile |  |
| ---- | ---- |
| Device type | phone |
| Resolution | 720 x 1280 |
| DPI | 160 |