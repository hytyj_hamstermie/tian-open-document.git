# OpenHarmony系统应用

# 开发环境

- 系统版本：OpenHarmony 3.0LTS / OpenHarmony 3.1beta
- 开发板：3516 / rk3568
- IDE：DevEco3.0.0.800

# 开源系统应用

- SystemUI

  SystemUI应用是OpenHarmony中预置的系统应用，为用户提供系统相关信息展示及交互界面，包括系统状态、系统提示、系统提醒等，例如系统时间、电量信息。

  https://gitee.com/openharmony/applications_systemui

- Settings

  设置应用是 OpenHarmony 系统中预置的系统应用，为用户提供设置系统属性的交互界面，例如设置系统时间，屏幕亮度等系统属性。

  https://gitee.com/openharmony/applications_settings

- Launcher

  Launcher 作为系统人机交互的首要入口，提供应用图标的显示、点击启动、卸载应用，并提供桌面布局设置以及最近任务管理等功能。
  Launcher 采用纯 JS 语言开发，开发过程中不涉及任何 Java 部分的代码。

  https://gitee.com/openharmony/applications_launcher

# 导入工程

## 代码下载

```bash
git clone '复制的下载地址'
```

> 我本人喜欢用可视化TortoiseGit。
>

接下来我们拿SystemUI项目进行示例。

项目仓库：https://gitee.com/openharmony/applications_systemui

切换分支：与当前开发板系统保持一致。

> 使用其他分支可能由于每个版本系统底层实现修改而出现问题。
>

![s0](./figures/s0.png)

## 打开工程

选择Open File or Project，选择项目文件夹即可。

![](./figures/n0.png)

选择OK后，会弹出询问框。

![n1](./figures/n1.png)

选择`Update`。

再次弹出询问框。

![n2](./figures/n2.png)

选择`Trust Project`。

等待右下角gradle build进度完成。

![n3](./figures/n3.png)

提示报错。

![n4](./figures/n4.png)

根据错误提示，我们打开build.gradle。

![n5](./figures/n5.png)

将build.gradle中所有`http`开头的链接都修改为`https`。

```bash
buildscript {
    repositories {
        maven {
            url 'https://repo.ark.tools.huawei.com/artifactory/maven-public/'
        }
        ...
    }
    dependencies {
        classpath 'com.huawei.ohos:hap:3.0.5.2'
    }
}

allprojects {
    repositories {
        maven {
            url 'https://repo.ark.tools.huawei.com/artifactory/maven-public/'
        }
        ...
    }
}
```

修改时会弹出询问框，选择要OK。

![n6](./figures/n6.png)

选择重新同步工程。

![n7](./figures/n7.png)

终于可以看到工程目录了。

![n8](./figures/n8.png)

### 检查build.gradle文件

每次修改该文件后，会提示要求重新同步，选择进行同步即可。

![syncgradle](./figures/syncgradle.png)

### 检查local.properties文件

例如settings工程该文件可能会出现内容错乱。

![localerror](./figures/localerror.png)

删除不需要的内容，我们只需要配置：

- sdk.dir
- nodejs.dir
- npm.dir

![localright](./figures/localright.png)



### 执行Build

会发现多了很多警告，导致Build失败。

![n9](./figures/n9.png)

我们按照错误一个一个进行修改。

-  `\entry\src\main\config.json`
- `\product\navigationBar\src\main\config.json`
- `\product\notificationmanagement\src\main\config.json`
- `\product\statusbar\src\main\config.json` 

在配置中添加installationFree

```javascript
...
},
"distro": {
   ...
   "installationFree": true  
},
...
```

重新Build，出现了新的错误。

![n10](./figures/n10.png)

继续按照错误一个一个进行修改。发现引起错误的原因是由于资源文件内容都是空。

那么我们一个一个将资源文件补充进去。

- `\features\airplanecomponent\src\main\resources\base\element\string.json`

- `\features\airplanecomponent\src\main\resources\base\element\color.json`

- `\features\capsulecomponent\src\main\resources\base\element\string.json`

- `\features\wificomponent\src\main\resources\base\element\color.json`

- `\features\statusbarcomponent\src\main\resources\base\element\string.json`
- `\features\statusbarcomponent\src\main\resources\en_US\element\string.json`
- `\features\statusbarcomponent\src\main\resources\zh_CN\element\string.json`

```javascript
// 这样的资源文件会引起报错
{
  "string": [
    
  ]
}

// 随便补充内容即可
{
  "string": [
    {
      "name": "xxx",
      "value": "xxx"
    }
  ]
}
```

可以在工程的build目录下查看到所有未签名的hap包。

![n15](./figures/n15.png)



# 应用签名

> 需要使用的别名与密码：
>
> - Alias：OpenHarmony Application Release
> - Password：123456
>

## 生成签名文件

查看了官方的签名工具仓：https://gitee.com/openharmony/signcenter_tool

发现官方就是没提供.cer，只好自己生成一个。在工具仓你可以获取到一个OpenHarmony.p12的文件。

```bash
## .csr
keytool -certreq -alias "OpenHarmony Application Release" -keystore E:\signcenter_tool\key\OpenHarmony.p12 -storetype pkcs12 -file E:\signcenter_tool\key\OpenHarmony.csr

## .cer
## 需要进入Openharmony SDK路径/toolchain/lib目录下执行
keytool -gencert -alias "OpenHarmony Application CA" -infile E:\signcenter_tool\key\OpenHarmony.csr -outfile E:\signcenter_tool\key\OpenHarmony.cer -keystore OpenHarmony.p12 -sigalg SHA384withECDSA -storepass 123456 -ext KeyUsage:"critical=digitalSignature" -validity 3650 -rfc
```

## 获取应用 Profile 文件

每个官方开源工程下都有个signature文件夹，直接使用里面的p7b文件即可。

> 也可以重新生成，自行生成参考官方文档。

![n11](./figures/n11.png)

## 配置签名

打开Project Strcture->Modules->Signing Configs，去掉勾选Automatically generate signing，填写签名配置。

> 执行Apply后会弹出提示框，忽略即可。

![n12](./figures/n12.png)

需要配置签名的Modules如下：

- navigationBar
- statusbar
- systemDialog(3.1beta已去掉)

执行Build，即可在工程build目录下看到已签名完成的hap包。

![n14](./figures/n14.png)

# 应用安装

> hdc_std使用指导：https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/subsystems/subsys-toolchain-hdc-guide.md

## OpenHarmony 3.0LTS

### 临时安装

使用一般hap包安装方式，适合临时调试。清除设备中存储的应用信息后，系统应用将启用原来预置的系统包。

```bash
hdc_std install [-r/-d/-g] 应用路径
```

### 内置安装

```shell
# 进入交互命令环境
hdc_std shell
```

如果设备不存在 `/system/app` 目录，则需要手动创建该目录并修改权限。

```shell
$ cd system
$ mkdir app
$ chmod 777 app
```

`/system/app` 目录放置系统应用，例如：Launcher，SystemUI，Settings 等。

发送hap包到目录。

```bash
hdc_std file send [文件路径]\[应用].hap system/app/[应用].hap
```

新加入的hap包需要在该目录下手动设置权限。

```shell
$ chmod 666 hap包名
```

每次系统重启，都会自动拉起该目录下的所有应用。

```bash
$ reboot
```

注意，如果设备之前安装过系统应用，则需要执行如下两条命令清除设备中存储的应用信息才能够在设备重启的时候将我们装入设备的新 hap 包正常拉起。

```bash
hdc_std  shell rm -rf /data/misc_de/0/mdds/0/default/bundle_manager_service

hdc_std  shell rm -rf  /data/accounts
```

## OpenHarmony 3.1beta

### 临时安装

同OpenHarmony 3.0LTS临时安装操作。

> 3.1beta临时安装可能会出现界面不刷新的情况，建议是安装系统应用后，进行重启。

```bash
# 安装应用
hdc_std install [-r/-d/-g] 应用路径

# 成功安装后，3.1beta需要重启系统
hdc_std shell reboot
```

### 内置安装

进入交互命令环境。

```bash
hdc_std shell
```

3.1beta每次系统开启后，需要先更改目录权限，才能进行操作。

```shell
# 更改权限
$ mount -o remount,rw /   
# 完成后退出
$ exit
```

后续安装操作同OpenHarmony 3.0LTS内置安装操作。

完成后重启系统即可。

```bash
hdc_std shell rm -rf /data/misc_de/0/mdds/0/default/bundle_manager_service

hdc_std shell rm -rf  /data/accounts

hdc_std shell reboot
```

