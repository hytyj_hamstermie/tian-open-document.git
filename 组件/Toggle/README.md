# Toggle

# 支持版本

OpenHarmony 3.1beta

SDK Version 8

# 子组件

可以包含子组件。

> Checkbox、Switch子组件会渲染在控件之后，推荐使用Row容器包裹。
>
> Button子组件会渲染在控件内。

# 接口

```typescript
Toggle(
	options: { 
		type: ToggleType, 
		isOn?: boolean
	}
)
```

- 参数

| 参数名 | 参数类型   | 必填 | 默认值 | 参数描述     |
| ------ | ---------- | ---- | ------ | ------------ |
| type   | ToggleType | 是   | 无     | 组件展现形式 |
| isOn   | boolean    | 否   | false  | 是否选中状态 |

- ToggleType枚举说明

| 名称     | 描述                                                         |
| :------- | :----------------------------------------------------------- |
| Checkbox | 复选框切换组件                                               |
| Switch   | 开关式切换组件                                               |
| Button   | 简单按钮式切换组件<br/>默认带圆角边（无法修改）<br/>高宽设置一样时，将展示圆形按钮。 |

# 事件

| 名称                             | 功能描述                       |
| :------------------------------- | :----------------------------- |
| onChange( isOn: boolean) => void | 当前组件选中状态变化时触发事件 |

# 特有属性

| 名称             | 参数说明      | 默认值 | 描述                                     |
| ---------------- | ------------- | ------ | ---------------------------------------- |
| selectedColor    | ResourceColor | -      | 当设置选中状态组件背景色 |
| switchPointColor | ResourceColor | -      | 当组件类型为Switch时，设置开关圆点背景色  |

# 属性

具有通用属性，这里只介绍使用率比较高的属性。

- 使用responseRegion属性可以模拟web label标签效果。
- 组件响应会自行切换状态，可以考虑使用touchable/enabled来限制。
- size与width&height属性会互相覆盖，后定义的属性会覆盖前一个。

| 名称   | 类型   |  描述 |
| ------ | ------ | ------ |
| width  | Length |设置组件自身的宽度<br/>`type`为`Button`必须设置宽度。|
| height | Length |设置组件自身的高度|
| **responseRegion** | Array<Rectangle> \| Rectangle | 设置当前组件的响应区域。<br/>区域x与y坐标基于渲染控件的最左上边缘。 |
| **size**       | {width?: Length,height?: Length} | 设置高宽尺寸。<br/>width与height设置一样时可以展现圆形按钮。 |
| **touchable** | boolean | 设置当前组件的可触摸性 |
| padding | {top?: Length,right?: Length,bottom?: Length,left?: Length} \| Length | 设置内边距属性。<br/>参数为Length类型时，四个方向内边距同时生效。 |
| margin | {top?: Length,right?: Length,bottom?: Length,left?: Length}\| Length | 设置外边距属性。<br/>参数为Length类型时，四个方向外边距同时生效。 |
| **backgroundColor** | Color | 设置背景色 |
| opacity         |           number \| Resource                                                   |                                                              |
| border | {width?: Length,color?: Color,radius?: Length,style?: BorderStyle} | 统一边框样式设置接口。<br/>设置边框会包含空出的区域。 |
| borderStyle | BorderStyle| 设置元素的边框样式。 |
| borderWidth | Length | 设置元素的边框宽度。 |
| borderColor | Color | 设置元素的边框颜色。 |
| borderRadius | Length | 设置元素的边框圆角半径。 |
| **enabled** | boolean | 值为true表示组件可用，可响应点击等操作；<br/>值为false时，不响应点击等操作。 |
| aspectRatio | number | 组件进行缩放 |
| **clip** |  Shape \| boolean | `type`为`Button`时可以进行裁剪 |

# 复选框Checkbox

## 默认样式

![00](./figures/00.png)

## 特性

- 不指定宽高时，响应区域默认宽高为 74vp X 74vp，控件区域宽高为46vp X 46vp
- 复选框控件会始终根据当前所使用单位，在定义的宽高基础上，每边加上一个14单位的空间。比如，设置Toggle宽高为100vp X 100vp时，那么整个组件的实际宽高为128vp X 128vp。
- 使用`.padding(0)`可以去除默认空白区域
- 未选中状态默认边框无法通过`border`修改

或许当前我们可以尝试将样式覆盖上去：

![01](./figures/01.png)

```typescript
Toggle({
  type: ToggleType.Checkbox,
  isOn: false
})
    .selectedColor(Color.Red)
    .backgroundColor(Color.Red)
    .borderRadius(5)
    .padding(0)
    .margin(20)
    .border({
        width: 1
    })
```

## 示例

![1](./figures/1.png)

```typescript
ForEach(Array(4), (v, k) => {
    Row() {
      Toggle({
        type: ToggleType.Checkbox,
        isOn: k == 0 ? true : false
      }) {
        Text(`测试选项${k+1}`).fontSize(30)
      }
      .onChange((res) => {
        console.log(res.toString())
      })
      .selectedColor(Color.Red)
      .responseRegion({  //设置可操作区域为整行
        x: 0,
        y: 0,
        width: '720lpx',  //designWidth = 720；width设置为100%时，为控件的宽，这里为100% = 50 + 28 = 78；
        height: 100
      })
      .size({
        width: 50,
        height: 50
      })
    }
    .backgroundColor('#ddd')
    .width("100%")
    .height(100)

    Divider()
})
```

# 开关Switch

## 默认样式

![10](./figures/10.png)

## 特性

- 不指定宽高时，响应区域默认宽高为 60vp X 76vp，控件区域宽高为48vp X 26vp
- 开关控件会始终根据当前所使用单位，在定义的宽高基础上，左右加上一个6单位的空间，上下加上一个25单位的空间。比如，设置Toggle宽高为100vp X 100vp时，那么整个组件的实际宽高为112vp X 150vp。
- 使用`.width(60).padding(0)`，可以去除默认空白区域。调试时可以添加`border`属性来查看响应区域。
- 开关控件背景色无法修改。

或许当前我们可以尝试将样式覆盖上去：

![11](./figures/11.png)

```typescript
Toggle({
  type: ToggleType.Switch,
  isOn: false
})
    .padding(0)
    .width(60)
    .backgroundColor(Color.Pink)
    .borderRadius(20)
    .selectedColor(Color.Red)
```

## 示例

![2](./figures/2.png)

```typescript
ForEach(Array(4), (v, k) => {
  Flex({ alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
    Text(`测试选项${k + 1}`).flexGrow(1).fontSize('30lpx')
    Toggle({
      type: ToggleType.Switch,
      isOn: k == 0 ? true : false
    }).onChange((res) => {
      console.log(res.toString())
    })
    .selectedColor(Color.Yellow)
    .switchPointColor(Color.Brown)
    .size({
        width: '100lpx',
        height: '80lpx'
    })
  }
  .backgroundColor('#ddd')
  .padding({ left: 20, right: 20 })

  Divider()
})
```

# 按钮Button

## 默认样式

![20](./figures/20.png)

## 特性

- 需要指定一个宽度，默认宽度为0，默认高度为28vp。
- 按钮形式可以看做同`button`组件。
- 按钮控件背景色无法修改。

或许当前我们可以尝试将样式覆盖上去：

![21](./figures/21.png)

```typescript
Toggle({
  type: ToggleType.Button,
  isOn: false
})
    .width(80)
    .selectedColor(Color.Red)
    .backgroundColor(Color.Pink)
    .borderRadius(15)
```

## 示例

![3](./figures/3.png)

```typescript
ForEach(Array(4), (v, k) => {
  Toggle({
    type: ToggleType.Button,
    isOn: k == 0 ? true : false
  }) {
    Text(`测试选项${k + 1}`).flexGrow(1).fontSize(24).fontColor(Color.White)
  }
  .selectedColor(Color.Blue)
  .backgroundColor(Color.Green)
  .borderRadius(150)   
  .size({
    width: 150,
    height: 80
  })
  .onChange((res) => {
    console.log(res.toString())
  })
  .margin(10)
})
```
