# Windows x64

## 配置 JAVA 环境

### 安装 jdk

> https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html
>
> 请选择1.8版本

### 配置 jdk

找到自己的 jdk 安装路径：

> 例如：C:\Program Files\Java\jdk1.8.0_131

打开**我的电脑 > 属性 > 高级系统设置 > 环境变量**，在**用户变量**中新增变量 JAVA_HOME 值为该 jdk 安装目录。

然后，在系统变量 Path 中新增该 jdk 路径：

> %JAVA_HOME%\bin

启动 cmd 执行：

```bash
# 输入
java -version
```

看到以下内容配置成功。

![vjava](./figures/vjava.png)

找到自己的 jre 安装路径：

> 例如：C:\Program Files\Java\jre1.8.0_131\bin

打开**我的电脑 > 属性 > 高级系统设置 > 环境变量**，在**系统变量**Path 中新增该 jre 路径。

启动 cmd 执行：

```bash
# 输入
keytool -list -v -keystore debug.keystore
```

看到以下内容配置成功。

![vkeytool](./figures/vkeytool.png)

## 安装 DevEco Studio 3.0 Beta1

> 下载地址：https://developer.harmonyos.com/cn/develop/deveco-studio#download

当前最新版本 DevEco Studio 3.0.0.601 x64

## 配置 OpenHarmony SDK

> 官方文档地址：https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md
>
> 注：在工程的 local.properties 中可以查看当前工程使用的是 Openharmony SDK 还是 HarmonyOS SDK

按照文档操作即可。

想要查看 SDK 本地路径及安装 SDK，可在 DevEco 中**File > Setting > SDK Manager**中查看。

![ohsdk](./figures/ohsdk.png)

## 创建 OpenHarmony 工程

> 官方文档地址：https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/use-wizard-to-create-project.md
>
> 按照文档操作即可。
>
> 注：按文档选择 **[Standard]Empty Ability**模板创建的工程才会使用 OpenHarmony SDK

### 创建 APP

![newproject](./figures/newproject.png)

![standard](./figures/standard.png)

- **Project Type 选择：Application**
- **Compatible API Version 推荐选择：SDK: API Version 7**
- **Language 推荐选择：eTS**

![createapp](./figures/createapp.png)

## OpenHarmony 应用签名

![flow](./figures/flow.png)

> **官方文档地址**
>
> 生成密钥和证书请求文件参考文档：https://developer.harmonyos.com/cn/docs/documentation/doc-guides/publish_app-0000001053223745#section9752152162813
>
> 发布签名包参考文档：https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md
>
> 注 1：需要安装至 3516 板上的应用，不论是 OpenHarmony 应用还是 HarmonyOS 应用都需要签名。两种工程都可以采取以下方式进行签名。
>
> 注 2：通过 DevEco Studio 来生成密钥文件（.p12 文件）和证书请求文件（.csr 文件）时，请按照文档在**Generate Key**界面中将所有密钥信息填写完整，否则可能导致签名失败。简单测试建议直接使用命令行生成。
>
> 注 3：需要成功配置好 JAVA 环境。
>
> 注 4：需要成功配置好 OpenHarmony SDK。

### 签名准备

#### 使用 DevEco Studio 生成密钥和证书请求文件

1. 在主菜单栏点击**Build > Generate Key** **and CSR**。

   > 如果本地已有对应的密钥，无需新生成密钥，可以在**Generate Key**界面中点击下方的 Skip 跳过密钥生成过程，直接使用已有密钥生成证书请求文件。

2. 在**Key Store File**中，可以点击**Choose Existing**选择已有的密钥库文件（存储有密钥的.p12 文件）；如果没有密钥库文件，点击**New**进行创建。下面以新创建密钥库文件为例进行说明。

   ![gkeyandcsr](./figures/gkeyandcsr.png)

3. 在**Create Key Store**窗口中，填写密钥库信息后，点击**OK**。

   - **Key Store File**：选择密钥库文件存储路径。
   - **Password**：设置密钥库密码，必须由大写字母、小写字母、数字和特殊符号中的两种以上字符的组合，长度至少为 8 位。请记住该密码，后续签名配置需要使用。
   - **Confirm Password**：再次输入密钥库密码。

   ![gkey](./figures/gkey.png)

4. 在**Generate Key**界面中，继续填写密钥信息后，点击**Next**。

   - **Alias**：密钥的别名信息，用于标识密钥名称。请记住该别名，后续签名配置需要使用。
   - **Password**：密钥对应的密码，与密钥库密码保持一致，无需手动输入。
   - **Validity**：证书有效期，建议设置为 25 年及以上，覆盖应用的完整生命周期。
   - **Certificate【必须填写完整】**：输入证书基本信息，如组织、城市或地区、国家码等。

   ![fillcer](./figures/fillcer.png)

5. 在**Generate CSR**界面，选择密钥和设置 CSR 文件存储路径。

   ![gcsr](./figures/gcsr.png)

6. 点击**OK**按钮，创建 CSR 文件成功，可以在存储路径下获取生成的密钥库文件（.p12）和证书请求文件（.csr）。

   ![gkeyandcsrsuccess](./figures/gkeyandcsrsuccess.png)

#### 命令行生成密钥文件（.p12 文件）

```bash
keytool -genkeypair -alias "{{别名}}" -keyalg EC -sigalg SHA256withECDSA -dname "C=CN,O=HUAWEI,OU=HUAWEI IDE,CN={{别名}}" -keystore {{输出的.p12文件完整路径}} -storetype pkcs12 -validity 9125 -storepass {{设置密码}} -keypass {{设置密码}}

## 按要求替换{{XXXX}}即可
########################### 例如 #################################
keytool -genkeypair -alias "myapplication" -keyalg EC -sigalg SHA256withECDSA -dname "C=CN,O=HUAWEI,OU=HUAWEI IDE,CN=myapplication" -keystore D:\myapplication.p12 -storetype pkcs12 -validity 9125 -storepass abc12345 -keypass abc12345
```

**请记住配置的别名及密码。**

密码设置要求：至少 8 位、至少包含数字与英文。**建议按要求设置密码。**

> DevEco 配置密码要求
>
> The password must contain at least 8 characters and include any two of the following:
>
> 1. Lowercase letters: a-z
> 2. Uppercase letters : A-Z
> 3. Digits : 0-9
> 4. Special characters : "`~!@#$%^\*()-\_=+\|[{}];:'",.>/? \

#### 命令行生成证书请求文件（.csr 文件）

```bash
keytool -certreq -alias "{{别名}}" -keystore {{.p12密钥文件完整路径}} -storetype pkcs12 -file {{输出的.csr文件完整路径}}

## 按要求替换{{XXXX}}即可
########################### 例如 #################################
keytool -certreq -alias "myapplication" -keystore D:\myapplication.p12 -storetype pkcs12 -file D:\myapplication.csr
```

执行后会要求输入密码，输入生成密钥文件（.p12 文件）相同的配置密码即可。

#### 命令行生成应用证书文件（.cer 文件）

> 以下步骤参考文档即可，HarmonyOS 应用也可以使用该方式。
>
> https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md

1、首先进入**OpenHarmony SDK**安装目录。

一般默认安装在`C:\Users\admin\AppData\Local\OpenHarmony\Sdk`

然后进入`toolchains\lib`目录，在该目录下启动 cmd 命令行工具。

2、执行命令行

```bash
keytool -gencert -alias "OpenHarmony Application CA" -infile {{.csr文件完整路径}} -outfile {{输出的.cer文件完整路径}} -keystore OpenHarmony.p12 -sigalg SHA384withECDSA -storepass 123456 -ext KeyUsage:"critical=digitalSignature" -validity 3650 -rfc

## 按要求替换{{XXXX}}即可
########################### 例如 #################################
keytool -gencert -alias "OpenHarmony Application CA" -infile D:\myapplication.csr -outfile D:\myapplication.cer -keystore OpenHarmony.p12 -sigalg SHA384withECDSA -storepass 123456 -ext KeyUsage:"critical=digitalSignature" -validity 3650 -rfc
```

#### 命令行生成应用 Profile 文件（.p7b 文件）

1、首先进入 OpenHarmony SDK 安装目录。

一般默认安装在`C:\Users\admin\AppData\Local\OpenHarmony\Sdk`

然后进入`toolchains\lib`目录，在该目录下启动 cmd 命令行工具。

2、执行命令行

```bash
java -jar provisionsigtool.jar sign --in UnsgnedReleasedProfileTemplate.json --out {{输出的.p7b文件完整路径}} --keystore OpenHarmony.p12 --storepass 123456 --alias "OpenHarmony Application Profile Release" --sigAlg SHA256withECDSA --cert OpenHarmonyProfileRelease.pem --validity 365 --developer-id ohosdeveloper --bundle-name {{当前应用包名}}  --distribution-certificate {{生成.cer文件完整路径}}

## 按要求替换{{XXXX}}即可
## 注意应用包名一致，应用包名可在entry\src\config.json中查看bundleName
########################### 例如 #################################
java -jar provisionsigtool.jar sign --in UnsgnedReleasedProfileTemplate.json --out D:\myapplication.p7b --keystore OpenHarmony.p12 --storepass 123456 --alias "OpenHarmony Application Profile Release" --sigAlg SHA256withECDSA --cert OpenHarmonyProfileRelease.pem --validity 365 --developer-id ohosdeveloper --bundle-name com.example.myapplication  --distribution-certificate D:\myapplication.cer
```

### 配置签名

> 参考文档：https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md
>
> 按照文档操作即可。

打开 **File > Project Structure**，点击 **Project > Signing Configs > debug**窗口中，去除勾选“Automatically generate signing”，然后配置指定模块的调试签名信息。

![sign](./figures/sign.png)

### 生成签名包

最后执行**Build > Build Hap(s)/APP(s) > Build Hap(s)**，签名成功后，可以在`entry\build\outputs\hap`中看到签名包。

![hap](./figures/hap.png)

## hdc_std 安装应用

> 官方文档地址：https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/subsystems/subsys-toolchain-hdc-guide.md#%E6%B3%A8%E6%84%8F%E4%BA%8B%E9%A1%B9

### 配置 hdc_std

在 OpenHarmony SDK 安装目录下找到 hdc_std.exe 所在目录。

> 一般默认在：C:\Users\admin\AppData\Local\OpenHarmony\Sdk\toolchains\

打开**我的电脑 > 属性 > 高级系统设置 > 环境变量**，然后，在系统变量 Path 中新增该路径。

启动 cmd 执行：

```bash
hdc_std -v
```

看到以下内容则配置成功

![vhdcstd](./figures/vhdcstd.png)

### 连接 3516 开发板

```bash
hdc_std list targets
```

看到设备码则设备连接成功

![vdevice](./figures/vdevice.png)

### 安装应用

找到.hap 应用路径，执行：

```bash
hdc_std install [-r/-d/-g] 应用路径

# 注意：应用必须签名
# 例如：
hdc_std install D:\entry-debug-standard-ark-signed.hap
```

安装成功

![install](./figures/install.png)

### hdc_std 工作异常

可以执行"hdc kill"或者"hdc start -r"杀掉 hdc 服务或者重启 hdc 服务，然后再执行 hdc list targets 查看是否已经可以获取设备信息。
